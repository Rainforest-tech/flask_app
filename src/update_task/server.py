import cherrypy
from flask import Flask

CHERRYPY_CONFIG = {
    "engine.autoreload.on": False,
    "log.screen": True,
    "server.socket_port": 5000,
    "server.socket_host": "0.0.0.0",
    "server.max_request_body_size": 0,
}


app = Flask(__name__)

@app.route('/')
def index():
    return "Updated!"


def run_server() -> None:
   # Mount the WSGI callable object (app) on the root directory
   cherrypy.tree.graft(app, "/")

   # Set the configuration of the web server
   cherrypy.config.update(CHERRYPY_CONFIG)

   # Start the CherryPy WSGI web server
   cherrypy.engine.start()
   cherrypy.engine.block()

if __name__ == "__main__":
    run_server()
